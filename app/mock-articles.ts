import {Article} from "./interfaces/article";

export var ARTICLES: Article[] = [
    {
        id: 1,
        title: 'Tlac Olafa',
        perex: 'Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku',
        text: 'Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku',
        url: 'tlac-olafa',
        rating: 4,
        img: 'https://s3.amazonaws.com/media.fundable.com/profile-sections/29014/files/lulzbot-tazwithscreenlr-wt6l6rkn2i.jpg',
        tags: '3D printing, Hobby',
    },
    {
        id: 1,
        title: 'Stavba Mibara pokracuje',
        perex: 'Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku',
        text: 'Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku',
        url: 'stavba-mibara-pokracuje',
        rating: 4,
        img: 'https://s3.amazonaws.com/media.fundable.com/profile-sections/29014/files/lulzbot-tazwithscreenlr-wt6l6rkn2i.jpg',
        tags: '3D printing, Hobby',
    },
    {
        id: 1,
        title: 'Mibaro oziva',
        perex: 'Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku',
        text: 'Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku. Tlac Olafa na 3D tlaciarni. Pokus o vysoku presnost, a kratky postup popisany v clanku',
        url: 'mibaro-oziva',
        rating: 4,
        img: 'https://s3.amazonaws.com/media.fundable.com/profile-sections/29014/files/lulzbot-tazwithscreenlr-wt6l6rkn2i.jpg',
        tags: '3D printing, Hobby',
    }
];