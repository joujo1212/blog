System.register(['angular2/core', 'angular2/router', "./articles.service", "angular2/src/platform/browser/title", "../../constants.service", "../../interfaces/broadcaster"], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, articles_service_1, title_1, constants_service_1, broadcaster_1;
    var ArticlesComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (articles_service_1_1) {
                articles_service_1 = articles_service_1_1;
            },
            function (title_1_1) {
                title_1 = title_1_1;
            },
            function (constants_service_1_1) {
                constants_service_1 = constants_service_1_1;
            },
            function (broadcaster_1_1) {
                broadcaster_1 = broadcaster_1_1;
            }],
        execute: function() {
            ArticlesComponent = (function () {
                function ArticlesComponent(articlesService, title, broadcaster) {
                    var _this = this;
                    this.articlesService = articlesService;
                    this.title = title;
                    this.broadcaster = broadcaster;
                    title.setTitle(constants_service_1.MAIN_PAGE_TITLE);
                    // register subscribe of broadcaster
                    this.broadcaster.subscribe(function (value) { return _this.getArticles(value); });
                }
                ;
                ArticlesComponent.prototype.getArticles = function (searchPhrase) {
                    var _this = this;
                    this.articlesService.getArticles(searchPhrase, null)
                        .subscribe(function (items) { return _this.items = items; }, function (error) { return alert('Chyba servera, skúte to neskôr'); });
                };
                ArticlesComponent.prototype.ngOnInit = function () {
                    this.getArticles(null);
                };
                ArticlesComponent = __decorate([
                    core_1.Component({
                        selector: 'articles',
                        templateUrl: 'app/components/articles/articles.template.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        inputs: ['data']
                    }), 
                    __metadata('design:paramtypes', [articles_service_1.ArticlesService, title_1.Title, broadcaster_1.Broadcaster])
                ], ArticlesComponent);
                return ArticlesComponent;
            }());
            exports_1("ArticlesComponent", ArticlesComponent);
        }
    }
});
//# sourceMappingURL=articles.component.js.map