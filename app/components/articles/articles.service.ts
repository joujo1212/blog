import {Injectable} from 'angular2/core';
import {ARTICLES} from "../../mock-articles";
import {Observable} from 'rxjs/Observable';
import {Http} from "angular2/http";
import {Article} from "../../interfaces/article";
import {SERVER_URL} from "../../constants.service";
import {wrapJsonRPC} from "../../utils";

@Injectable()
export class ArticlesService {
    constructor(private http:Http) {
    }

    getArticles(searchPhrase:string, tag:string) {
        return this.http.post(SERVER_URL, wrapJsonRPC('getAllArticles', {
                page: 1,
                numForPage: 10,
                searchPhrase: searchPhrase,
                tag: tag,
                token: localStorage.getItem('token')
            }))
            .map(res => res.json().result)
            .catch(this.logAndPassOn)
    }

    private logAndPassOn(error:Error) {
        console.error(error);
        return Observable.throw(error);
    }
}