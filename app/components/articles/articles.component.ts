import {Component, EventEmitter} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {ArticlesService} from "./articles.service";
import {Article} from "../../interfaces/article";
import {OnInit} from "angular2/core";
import {Title} from "angular2/src/platform/browser/title";
import {RouteData} from "angular2/router";
import {MAIN_PAGE_TITLE} from "../../constants.service";
import {AfterViewInit} from "angular2/core";
import {Broadcaster} from "../../interfaces/broadcaster";

@Component({
    selector: 'articles',
    templateUrl: 'app/components/articles/articles.template.html',
    directives: [ROUTER_DIRECTIVES],
    inputs: ['data']
})

export class ArticlesComponent implements OnInit {

    constructor(private articlesService:ArticlesService, private title:Title, private broadcaster: Broadcaster) {
        title.setTitle(MAIN_PAGE_TITLE);
        // register subscribe of broadcaster
        this.broadcaster.subscribe(value => this.getArticles(value));
    };

    public items:Article[];

    getArticles(searchPhrase: string) {
        this.articlesService.getArticles(searchPhrase, null)
            .subscribe(
                items => this.items = items,
                error => alert('Chyba servera, skúte to neskôr'));
    }

    ngOnInit() {
        this.getArticles(null);
    }

}