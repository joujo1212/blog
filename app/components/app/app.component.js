System.register(['angular2/core', 'angular2/router', '../articles/articles.component', '../article/article.component', '../header/header.component', "../footer/footer.component", "../articles/articles.service", "../article/article.service", "angular2/src/platform/browser/title", "../add/add.component", "angular2/http", "../add/add.service", "../chat/chat.component", "../login/login.component", "../login/login.service", "../../interfaces/broadcaster", "../../interfaces/loginEvent"], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, articles_component_1, article_component_1, header_component_1, footer_component_1, articles_service_1, article_service_1, title_1, add_component_1, http_1, add_service_1, chat_component_1, login_component_1, login_service_1, broadcaster_1, loginEvent_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (articles_component_1_1) {
                articles_component_1 = articles_component_1_1;
            },
            function (article_component_1_1) {
                article_component_1 = article_component_1_1;
            },
            function (header_component_1_1) {
                header_component_1 = header_component_1_1;
            },
            function (footer_component_1_1) {
                footer_component_1 = footer_component_1_1;
            },
            function (articles_service_1_1) {
                articles_service_1 = articles_service_1_1;
            },
            function (article_service_1_1) {
                article_service_1 = article_service_1_1;
            },
            function (title_1_1) {
                title_1 = title_1_1;
            },
            function (add_component_1_1) {
                add_component_1 = add_component_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (add_service_1_1) {
                add_service_1 = add_service_1_1;
            },
            function (chat_component_1_1) {
                chat_component_1 = chat_component_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (broadcaster_1_1) {
                broadcaster_1 = broadcaster_1_1;
            },
            function (loginEvent_1_1) {
                loginEvent_1 = loginEvent_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                }
                ;
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: 'app/components/app/app.template.html',
                        directives: [router_1.ROUTER_DIRECTIVES, articles_component_1.ArticlesComponent, article_component_1.ArticleComponent, header_component_1.HeaderComponent, footer_component_1.FooterComponent, add_component_1.AddComponent, chat_component_1.ChatComponent, login_component_1.LoginComponent],
                        providers: [broadcaster_1.Broadcaster, loginEvent_1.LoginEvent, add_service_1.AddArticleService, articles_service_1.ArticlesService, article_service_1.ArticleService, login_service_1.LoginService, title_1.Title, http_1.HTTP_PROVIDERS],
                    }),
                    router_1.RouteConfig([
                        { path: '/', as: 'Articles', component: articles_component_1.ArticlesComponent, useAsDefault: true, data: { title: '' } },
                        { path: '/a/:url', as: 'ArticleDetail', component: article_component_1.ArticleComponent, data: { title: '' } },
                        { path: '/a/:url/edit', as: 'ArticleEdit', component: add_component_1.AddComponent, data: { title: 'Uprava clanku ', action: 'edit' } },
                        { path: '/add', as: 'AddArticle', component: add_component_1.AddComponent, data: { title: 'Novy clanok ', action: 'add' } },
                        { path: '/login', as: 'Login', component: login_component_1.LoginComponent, data: { title: 'Prihlasenie ' } },
                    ]), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map