import {Component, EventEmitter} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {ArticlesComponent} from '../articles/articles.component';
import {ArticleComponent} from '../article/article.component';
import {HeaderComponent} from '../header/header.component';
import {FooterComponent} from "../footer/footer.component";
import {ArticlesService} from "../articles/articles.service";
import {ArticleService} from "../article/article.service";
import {Title} from "angular2/src/platform/browser/title";
import {AddComponent} from "../add/add.component";
import {HTTP_PROVIDERS} from "angular2/http";
import {Injectable} from "angular2/core";
import {AddArticleService} from "../add/add.service";
import {ComponentInstruction} from "angular2/router";
import {OnDeactivate} from "angular2/router";
import {ChatComponent} from "../chat/chat.component";
import {LoginComponent} from "../login/login.component";
import {LoginService} from "../login/login.service";
import {Broadcaster} from "../../interfaces/broadcaster";
import {LoginEvent} from "../../interfaces/loginEvent";


@Component({
    selector: 'my-app',
    templateUrl: 'app/components/app/app.template.html',
    directives: [ROUTER_DIRECTIVES, ArticlesComponent, ArticleComponent, HeaderComponent, FooterComponent, AddComponent, ChatComponent, LoginComponent],
    providers: [Broadcaster, LoginEvent, AddArticleService, ArticlesService, ArticleService, LoginService, Title, HTTP_PROVIDERS],
})

@RouteConfig([
    {path: '/', as: 'Articles', component: ArticlesComponent, useAsDefault: true, data: {title: ''}},
    {path: '/a/:url', as: 'ArticleDetail', component: ArticleComponent, data: {title: ''}},
    {path: '/a/:url/edit', as: 'ArticleEdit', component: AddComponent, data: {title: 'Uprava clanku ', action: 'edit'}},
    {path: '/add', as: 'AddArticle', component: AddComponent, data: {title: 'Novy clanok ', action: 'add'}},
    {path: '/login', as: 'Login', component: LoginComponent, data: {title: 'Prihlasenie '}},
])

export class AppComponent {
    constructor() {
    };


}