System.register(['angular2/core', "angular2/router", "angular2/src/platform/browser/title", "./add.service", "../article/article.service", 'ng2-uploader', "../../constants.service"], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, router_2, title_1, router_3, add_service_1, article_service_1, ng2_uploader_1, constants_service_1;
    var AddComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
                router_2 = router_1_1;
                router_3 = router_1_1;
            },
            function (title_1_1) {
                title_1 = title_1_1;
            },
            function (add_service_1_1) {
                add_service_1 = add_service_1_1;
            },
            function (article_service_1_1) {
                article_service_1 = article_service_1_1;
            },
            function (ng2_uploader_1_1) {
                ng2_uploader_1 = ng2_uploader_1_1;
            },
            function (constants_service_1_1) {
                constants_service_1 = constants_service_1_1;
            }],
        execute: function() {
            AddComponent = (function () {
                function AddComponent(params, title, data, addArticleService, router, articleService) {
                    this.params = params;
                    this.title = title;
                    this.data = data;
                    this.addArticleService = addArticleService;
                    this.router = router;
                    this.articleService = articleService;
                    // instantiate interface to empty object - this way
                    this.article = {};
                    this.password = '';
                    this.options = {
                        url: constants_service_1.ENDPOINT_UPLOAD_IMAGE
                    };
                    title.setTitle(data.get('title'));
                    if (data.get('action') == 'edit') {
                        this.getArticle();
                    }
                }
                ;
                AddComponent.prototype.handleUpload = function (data) {
                    if (data && data.response) {
                        data = JSON.parse(data.response);
                        this.article.img = data.result[0];
                        this.uploadFile = data;
                    }
                };
                AddComponent.prototype.getArticle = function () {
                    var _this = this;
                    this.articleService.getArticleByUrl(this.params.get('url'))
                        .subscribe(function (article) {
                        if (article) {
                            _this.article = article;
                        }
                        else {
                            _this.router.navigate(['Articles']);
                        }
                    }, function (error) { return alert('Server error. Try again later'); });
                };
                AddComponent.prototype.addArticle = function () {
                    var _this = this;
                    this.addArticleService.addArticle(this.article)
                        .subscribe(function (resp) { return _this.router.navigate(['ArticleDetail', { url: resp }]); }, function (error) { return alert(JSON.parse(error._body).error.message); });
                };
                AddComponent = __decorate([
                    core_1.Component({
                        selector: 'add',
                        templateUrl: 'app/components/add/add.template.html',
                        directives: [ng2_uploader_1.UPLOAD_DIRECTIVES],
                    }), 
                    __metadata('design:paramtypes', [router_1.RouteParams, title_1.Title, router_3.RouteData, add_service_1.AddArticleService, router_2.Router, article_service_1.ArticleService])
                ], AddComponent);
                return AddComponent;
            }());
            exports_1("AddComponent", AddComponent);
        }
    }
});
//# sourceMappingURL=add.component.js.map