import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {Article} from "../../interfaces/article";
import {OnInit} from "angular2/core";
import {Input} from "angular2/core";
import {RouteParams} from "angular2/router";
import {Router} from "angular2/router";
import {Title} from "angular2/src/platform/browser/title";
import {RouteData} from "angular2/router";
import {TITLE_POSTFIX} from "../../constants.service.ts";
import {AddArticleService} from "./add.service";
import {ArticleService} from "../article/article.service";
import {UPLOAD_DIRECTIVES} from 'ng2-uploader';
import {ENDPOINT_UPLOAD_IMAGE} from "../../constants.service";

@Component({
    selector: 'add',
    templateUrl: 'app/components/add/add.template.html',
    directives: [UPLOAD_DIRECTIVES],

})

export class AddComponent {
    constructor(private params:RouteParams, private title:Title, private data:RouteData, private addArticleService: AddArticleService, private router: Router, private articleService: ArticleService) {
        title.setTitle(data.get('title'));
        if (data.get('action') == 'edit') {
            this.getArticle();
        }
    };

    // instantiate interface to empty object - this way
    public article:Article = <Article>{};
    public password: string = '';

    uploadFile: any;
    options: Object = {
        url: ENDPOINT_UPLOAD_IMAGE
    };

    handleUpload(data): void {
        if (data && data.response) {
            data = JSON.parse(data.response);
            this.article.img = data.result[0];
            this.uploadFile = data;
        }
    }

    getArticle() {
        this.articleService.getArticleByUrl(this.params.get('url'))
            .subscribe(
                article => {
                    if (article) {
                        this.article = article
                    } else {
                        this.router.navigate(['Articles'])
                    }
                },
                error => alert('Server error. Try again later')
            );
    }

    public addArticle() {
        this.addArticleService.addArticle(this.article)
            .subscribe(
                resp => this.router.navigate(['ArticleDetail', {url: resp}]),
                error => alert(JSON.parse(error._body).error.message));
    }

}