import {Injectable} from 'angular2/core';
import {ARTICLES} from "../../mock-articles";
import {wrapJsonRPC} from "../../utils";
import {SERVER_URL} from "../../constants.service";
import {Http} from "angular2/http";
import {Observable} from "rxjs/Observable";
import {Article} from "../../interfaces/article";

@Injectable()
export class AddArticleService {

    constructor(private http:Http) {
    }

    addArticle(article: Article) {
        return this.http.post(SERVER_URL, wrapJsonRPC('addArticle', {article: article, token: localStorage.getItem('token')}))
            .map(res => res.json().result)
            .catch(this.logAndPassOn);
    }

    deleteArticle(id: number) {
        return this.http.post(SERVER_URL, wrapJsonRPC('deleteArticle', {id: id, token: localStorage.getItem('token')}))
            .map(res => res.json().result)
            .catch(this.logAndPassOn);
    }

    private logAndPassOn(error:Error) {
        console.error(error);
        return Observable.throw(error);
    }
}