import {Injectable} from 'angular2/core';
import {ARTICLES} from "../../mock-articles";
import {wrapJsonRPC} from "../../utils";
import {SERVER_URL} from "../../constants.service";
import {Http} from "angular2/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ArticleService {

    constructor(private http:Http) {
    }

    getArticleByUrl(url: string) {
        return this.http.post(SERVER_URL, wrapJsonRPC('getArticleByUrl', {url: url}))
            .map(res => res.json().result)
            .catch(this.logAndPassOn);
    }

    private logAndPassOn(error:Error) {
        console.error(error);
        return Observable.throw(error);
    }
}