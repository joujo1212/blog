import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {Article} from "../../interfaces/article";
import {ArticleService} from "./article.service";
import {OnInit} from "angular2/core";
import {Input} from "angular2/core";
import {RouteParams} from "angular2/router";
import {Router} from "angular2/router";
import {Title} from "angular2/src/platform/browser/title";
import {RouteData} from "angular2/router";
import {TITLE_POSTFIX} from "../../constants.service";
import {ComponentInstruction} from "angular2/router";
import {OnDeactivate} from "angular2/router";
import {BrowserDomAdapter} from 'angular2/platform/browser'
import {AddArticleService} from "../add/add.service";
import {LoginService} from "../login/login.service";
import {ElementRef} from "angular2/core";
import {ENDPOINT_UPLOAD_IMAGE} from "../../constants.service";
import {UPLOAD_DIRECTIVES} from 'ng2-uploader';

declare var ContentTools:any;


@Component({
    selector: 'article',
    templateUrl: 'app/components/article/article.template.html',
    directives: [UPLOAD_DIRECTIVES],
    providers: [BrowserDomAdapter]
})

export class ArticleComponent implements OnInit, OnDeactivate {
    constructor(private articleService:ArticleService, private params:RouteParams, private router:Router, private title:Title, private data:RouteData, private _dom:BrowserDomAdapter, public  addArticleService:AddArticleService, private loginService:LoginService) {
        title.setTitle(data.get('title'));
    };

    public article:Article = <Article>{};
    public password:string = '';
    //public canEdit = this.loginService.isLogged();
    public canEdit = false;
    uploadFile:any;
    options:Object = {
        url: ENDPOINT_UPLOAD_IMAGE
    };

    getArticle() {
        this.articleService.getArticleByUrl(this.params.get('url'))
            .subscribe(
                article => {
                    if (article) {
                        this.article = article;
                        if (this.loginService.isLogged()) {
                            var editor;
                            var editorCls = ContentTools.EditorApp.getCls();
                            editor = ContentTools.EditorApp.get();
                            editor.init('*[data-editable]', 'data-name');
                            editor.bind('save', regions => this.addArticle(regions['main-content']));
                            editor.start = function () {
                                editorCls.prototype.start.call(this);
                                editor.trigger('start');
                            };

                            editor.stop = function () {
                                editorCls.prototype.stop.call(this);
                                editor.trigger('stop');
                            };
                            editor.bind('start', element => this.canEdit = true);
                            editor.bind('stop', element => this.canEdit = false);
                        }
                    } else {
                        this.router.navigate(['Articles']);
                    }
                },
                error => alert('Server error. Try again later')
            );
    }

    public deleteArticle() {
        this.addArticleService.deleteArticle(this.article.id).subscribe(
            result => this.router.navigate(['Articles']),
            error => alert(JSON.parse(error._body).error.message))
        );
    }

    public addArticle(content) {
        // Workaround - for some reasongs Content-Tools don't return content if it's not changed
        if (!content) {
            this.article.text = document.querySelector('[data-name=main-content]').innerHTML;
        } else {
            this.article.text = content;
        }
        var title = document.querySelector('.article-title');
        this.article.title = title.innerText;
        var perex = document.querySelector('.article-perex');
        this.article.perex = perex.innerText;
        this.addArticleService.addArticle(this.article)
            .subscribe(
                resp => this.router.navigate(['ArticleDetail', {url: resp}]),
                error => alert(JSON.parse(error._body).error.message));

    }

    handleUpload(data):void {
        if (data && data.response) {
            data = JSON.parse(data.response);
            this.article.img = data.result[0];
            this.uploadFile = data;
        }
    }

    ngOnInit() {
        this.getArticle();
    }

    routerOnDeactivate(next:ComponentInstruction, prev:ComponentInstruction) {
        var editor;
        editor = ContentTools.EditorApp.get();
        editor.destroy();
        console.log(
            `Navigating from "${prev ? prev.urlPath : 'null'}" to "${next.urlPath}"`);
    }
}