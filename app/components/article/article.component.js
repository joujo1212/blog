System.register(['angular2/core', "./article.service", "angular2/router", "angular2/src/platform/browser/title", 'angular2/platform/browser', "../add/add.service", "../login/login.service", "../../constants.service", 'ng2-uploader'], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, article_service_1, router_1, router_2, title_1, router_3, browser_1, add_service_1, login_service_1, constants_service_1, ng2_uploader_1;
    var ArticleComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (article_service_1_1) {
                article_service_1 = article_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
                router_2 = router_1_1;
                router_3 = router_1_1;
            },
            function (title_1_1) {
                title_1 = title_1_1;
            },
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (add_service_1_1) {
                add_service_1 = add_service_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (constants_service_1_1) {
                constants_service_1 = constants_service_1_1;
            },
            function (ng2_uploader_1_1) {
                ng2_uploader_1 = ng2_uploader_1_1;
            }],
        execute: function() {
            ArticleComponent = (function () {
                function ArticleComponent(articleService, params, router, title, data, _dom, addArticleService, loginService) {
                    this.articleService = articleService;
                    this.params = params;
                    this.router = router;
                    this.title = title;
                    this.data = data;
                    this._dom = _dom;
                    this.addArticleService = addArticleService;
                    this.loginService = loginService;
                    this.article = {};
                    this.password = '';
                    //public canEdit = this.loginService.isLogged();
                    this.canEdit = false;
                    this.options = {
                        url: constants_service_1.ENDPOINT_UPLOAD_IMAGE
                    };
                    title.setTitle(data.get('title'));
                }
                ;
                ArticleComponent.prototype.getArticle = function () {
                    var _this = this;
                    this.articleService.getArticleByUrl(this.params.get('url'))
                        .subscribe(function (article) {
                        if (article) {
                            _this.article = article;
                            if (_this.loginService.isLogged()) {
                                var editor;
                                var editorCls = ContentTools.EditorApp.getCls();
                                editor = ContentTools.EditorApp.get();
                                editor.init('*[data-editable]', 'data-name');
                                editor.bind('save', function (regions) { return _this.addArticle(regions['main-content']); });
                                editor.start = function () {
                                    editorCls.prototype.start.call(this);
                                    editor.trigger('start');
                                };
                                editor.stop = function () {
                                    editorCls.prototype.stop.call(this);
                                    editor.trigger('stop');
                                };
                                editor.bind('start', function (element) { return _this.canEdit = true; });
                                editor.bind('stop', function (element) { return _this.canEdit = false; });
                            }
                        }
                        else {
                            _this.router.navigate(['Articles']);
                        }
                    }, function (error) { return alert('Server error. Try again later'); });
                };
                ArticleComponent.prototype.deleteArticle = function () {
                    var _this = this;
                    this.addArticleService.deleteArticle(this.article.id).subscribe(function (result) { return _this.router.navigate(['Articles']); }, function (error) { return alert(JSON.parse(error._body).error.message); });
                    ;
                };
                ArticleComponent.prototype.addArticle = function (content) {
                    var _this = this;
                    // Workaround - for some reasongs Content-Tools don't return content if it's not changed
                    if (!content) {
                        this.article.text = document.querySelector('[data-name=main-content]').innerHTML;
                    }
                    else {
                        this.article.text = content;
                    }
                    var title = document.querySelector('.article-title');
                    this.article.title = title.innerText;
                    var perex = document.querySelector('.article-perex');
                    this.article.perex = perex.innerText;
                    this.addArticleService.addArticle(this.article)
                        .subscribe(function (resp) { return _this.router.navigate(['ArticleDetail', { url: resp }]); }, function (error) { return alert(JSON.parse(error._body).error.message); });
                };
                ArticleComponent.prototype.handleUpload = function (data) {
                    if (data && data.response) {
                        data = JSON.parse(data.response);
                        this.article.img = data.result[0];
                        this.uploadFile = data;
                    }
                };
                ArticleComponent.prototype.ngOnInit = function () {
                    this.getArticle();
                };
                ArticleComponent.prototype.routerOnDeactivate = function (next, prev) {
                    var editor;
                    editor = ContentTools.EditorApp.get();
                    editor.destroy();
                    console.log("Navigating from \"" + (prev ? prev.urlPath : 'null') + "\" to \"" + next.urlPath + "\"");
                };
                ArticleComponent = __decorate([
                    core_1.Component({
                        selector: 'article',
                        templateUrl: 'app/components/article/article.template.html',
                        directives: [ng2_uploader_1.UPLOAD_DIRECTIVES],
                        providers: [browser_1.BrowserDomAdapter]
                    }), 
                    __metadata('design:paramtypes', [article_service_1.ArticleService, router_1.RouteParams, router_2.Router, title_1.Title, router_3.RouteData, browser_1.BrowserDomAdapter, add_service_1.AddArticleService, login_service_1.LoginService])
                ], ArticleComponent);
                return ArticleComponent;
            }());
            exports_1("ArticleComponent", ArticleComponent);
        }
    }
});
//# sourceMappingURL=article.component.js.map