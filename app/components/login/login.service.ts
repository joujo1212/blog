import {Injectable} from 'angular2/core';
import {ARTICLES} from "../../mock-articles";
import {wrapJsonRPC} from "../../utils";
import {SERVER_URL} from "../../constants.service";
import {Http} from "angular2/http";
import {Observable} from "rxjs/Observable";
import {Article} from "../../interfaces/article";

declare var CryptoJS:any;


@Injectable()
export class LoginService {

    constructor(private http:Http) {
    }

    login(password:string) {
        var passhash = CryptoJS.SHA1(password);
        return this.http.post(SERVER_URL, wrapJsonRPC('login', {pass: passhash + ""}))
            .map(res => {res.json().result; localStorage.setItem('token', res.json().result.token);})
            .catch(this.logAndPassOn);
    }

    private logAndPassOn(error:Error) {
        console.error(error);
        return Observable.throw(error);
    }

    isLogged() {
        return localStorage.getItem('token') !== null;
    }

    logout() {
        return this.http.post(SERVER_URL, wrapJsonRPC('logout', {token: localStorage.getItem('token')}))
            .map(res => localStorage.removeItem('token'))
            .catch(this.logAndPassOn);
    }
}