System.register(['angular2/core', "angular2/router", "angular2/src/platform/browser/title", "./login.service", "../../interfaces/loginEvent"], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, title_1, login_service_1, loginEvent_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (title_1_1) {
                title_1 = title_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (loginEvent_1_1) {
                loginEvent_1 = loginEvent_1_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                function LoginComponent(title, loginService, router, loginEvent) {
                    this.title = title;
                    this.loginService = loginService;
                    this.router = router;
                    this.loginEvent = loginEvent;
                    this.logining = false;
                    this.password = '';
                    this.showMessageBox = false;
                }
                ;
                LoginComponent.prototype.login = function () {
                    var _this = this;
                    if (!this.logining) {
                        this.logining = true;
                        this.loginService.login(this.password).subscribe(function (res) {
                            _this.logining = false;
                            _this.router.navigate(['Articles']);
                            _this.loginEvent.emit(true);
                        }, function (error) {
                            setTimeout(function (any) {
                                alert(JSON.parse(error._body).error.message);
                                _this.logining = false;
                            }, 1000);
                        });
                    }
                };
                LoginComponent.prototype.toggleMessageBox = function () {
                    this.showMessageBox = !this.showMessageBox;
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        selector: 'login',
                        templateUrl: 'app/components/login/login.template.html'
                    }), 
                    __metadata('design:paramtypes', [title_1.Title, login_service_1.LoginService, router_1.Router, loginEvent_1.LoginEvent])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
//# sourceMappingURL=login.component.js.map