System.register(['angular2/core', "../../utils", "../../constants.service", "angular2/http", "rxjs/Observable"], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, utils_1, constants_service_1, http_1, Observable_1;
    var LoginService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (utils_1_1) {
                utils_1 = utils_1_1;
            },
            function (constants_service_1_1) {
                constants_service_1 = constants_service_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            }],
        execute: function() {
            LoginService = (function () {
                function LoginService(http) {
                    this.http = http;
                }
                LoginService.prototype.login = function (password) {
                    var passhash = CryptoJS.SHA1(password);
                    return this.http.post(constants_service_1.SERVER_URL, utils_1.wrapJsonRPC('login', { pass: passhash + "" }))
                        .map(function (res) { res.json().result; localStorage.setItem('token', res.json().result.token); })
                        .catch(this.logAndPassOn);
                };
                LoginService.prototype.logAndPassOn = function (error) {
                    console.error(error);
                    return Observable_1.Observable.throw(error);
                };
                LoginService.prototype.isLogged = function () {
                    return localStorage.getItem('token') !== null;
                };
                LoginService.prototype.logout = function () {
                    return this.http.post(constants_service_1.SERVER_URL, utils_1.wrapJsonRPC('logout', { token: localStorage.getItem('token') }))
                        .map(function (res) { return localStorage.removeItem('token'); })
                        .catch(this.logAndPassOn);
                };
                LoginService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], LoginService);
                return LoginService;
            }());
            exports_1("LoginService", LoginService);
        }
    }
});
//# sourceMappingURL=login.service.js.map