import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {Article} from "../../interfaces/article";
import {OnInit} from "angular2/core";
import {Input} from "angular2/core";
import {RouteParams} from "angular2/router";
import {Router} from "angular2/router";
import {Title} from "angular2/src/platform/browser/title";
import {RouteData} from "angular2/router";
import {TITLE_POSTFIX} from "../../constants.service.ts";
import {ArticleService} from "../article/article.service";
import {UPLOAD_DIRECTIVES} from 'ng2-uploader';
import {ENDPOINT_UPLOAD_IMAGE} from "../../constants.service";
import {LoginService} from "./login.service";
import {LoginEvent} from "../../interfaces/loginEvent";

@Component({
    selector: 'login',
    templateUrl: 'app/components/login/login.template.html'
})

export class LoginComponent {
    constructor(private title:Title, private loginService:LoginService, private router: Router, private loginEvent: LoginEvent) {

    };

    public logining:boolean = false;
    public password:string = '';

    public login() {
        if (!this.logining) {
            this.logining = true;
            this.loginService.login(this.password).subscribe(
                res => {
                    this.logining = false;
                    this.router.navigate(['Articles']);
                    this.loginEvent.emit(true);
                },
                error => {
                    setTimeout(any => {
                            alert(JSON.parse(error._body).error.message);
                            this.logining = false;
                        },
                        1000
                    );
                }
            )
        }
    }

    private showMessageBox:boolean = false;

    public toggleMessageBox() {
        this.showMessageBox = !this.showMessageBox;
    }

}