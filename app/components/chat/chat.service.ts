import {Injectable} from 'angular2/core';
import {ARTICLES} from "../../mock-articles";
import {wrapJsonRPC} from "../../utils";
import {SERVER_URL} from "../../constants.service";
import {Http} from "angular2/http";
import {Observable} from "rxjs/Observable";
import {Article} from "../../interfaces/article";

@Injectable()
export class AddArticleService {

    constructor(private http:Http) {
    }

    addArticle(passhash: string, article: Article) {
        return this.http.post(SERVER_URL, wrapJsonRPC('addArticle', {article: article, pass: passhash}))
            .map(res => res.json().result)
            .catch(this.logAndPassOn);
    }

    private logAndPassOn(error:Error) {
        console.error(error);
        return Observable.throw(error);
    }
}