import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {Article} from "../../interfaces/article";
import {OnInit} from "angular2/core";
import {Input} from "angular2/core";
import {RouteParams} from "angular2/router";
import {Router} from "angular2/router";
import {Title} from "angular2/src/platform/browser/title";
import {RouteData} from "angular2/router";
import {TITLE_POSTFIX} from "../../constants.service.ts";
import {ArticleService} from "../article/article.service";
import {UPLOAD_DIRECTIVES} from 'ng2-uploader';
import {ENDPOINT_UPLOAD_IMAGE} from "../../constants.service";

@Component({
    selector: 'chat',
    templateUrl: 'app/components/chat/chat.template.html',

})

export class ChatComponent {
    constructor( private title:Title) {

    };

    private showMessageBox:boolean = false;

    public toggleMessageBox() {
        this.showMessageBox = !this.showMessageBox;
    }

}