System.register(['angular2/core', 'angular2/router', "../login/login.service", "../../interfaces/broadcaster", "../../interfaces/loginEvent", "../add/add.service", "angular2/router"], function(exports_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, login_service_1, broadcaster_1, loginEvent_1, add_service_1, router_2;
    var HeaderComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (broadcaster_1_1) {
                broadcaster_1 = broadcaster_1_1;
            },
            function (loginEvent_1_1) {
                loginEvent_1 = loginEvent_1_1;
            },
            function (add_service_1_1) {
                add_service_1 = add_service_1_1;
            },
            function (router_2_1) {
                router_2 = router_2_1;
            }],
        execute: function() {
            HeaderComponent = (function () {
                function HeaderComponent(loginService, loginEvent, broadcaster, addArticleService, router) {
                    var _this = this;
                    this.loginService = loginService;
                    this.loginEvent = loginEvent;
                    this.broadcaster = broadcaster;
                    this.addArticleService = addArticleService;
                    this.router = router;
                    this.searchPhrase = '';
                    this.showLogout = false;
                    this.showLogout = this.loginService.isLogged();
                    this.loginEvent.subscribe(function (value) { return _this.showLogout = value; });
                }
                HeaderComponent.prototype.search = function () {
                    console.log('Searching ' + this.searchPhrase);
                    this.broadcaster.emit(this.searchPhrase);
                };
                HeaderComponent.prototype.logout = function () {
                    var _this = this;
                    this.loginService.logout().subscribe(function (resp) { return _this.showLogout = false; }, function (error) { return console.log('Logout problem'); });
                };
                HeaderComponent.prototype.addEmptyArticle = function () {
                    var _this = this;
                    // new, empty article
                    var article = {};
                    this.addArticleService.addArticle(article).subscribe(function (res) { return _this.router.navigate(['ArticleDetail', { url: res }]); }, function (error) { return alert(JSON.parse(error._body).error.message); });
                };
                HeaderComponent = __decorate([
                    core_1.Component({
                        selector: 'header',
                        templateUrl: 'app/components/header/header.template.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                    }), 
                    __metadata('design:paramtypes', [login_service_1.LoginService, loginEvent_1.LoginEvent, broadcaster_1.Broadcaster, add_service_1.AddArticleService, router_2.Router])
                ], HeaderComponent);
                return HeaderComponent;
            }());
            exports_1("HeaderComponent", HeaderComponent);
        }
    }
});
//# sourceMappingURL=header.component.js.map