import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {LoginService} from "../login/login.service";
import {Broadcaster} from "../../interfaces/broadcaster";
import {LoginEvent} from "../../interfaces/loginEvent";
import {AddArticleService} from "../add/add.service";
import {Article} from "../../interfaces/article";
import {Router} from "angular2/router";

@Component({
    selector: 'header',
    templateUrl: 'app/components/header/header.template.html',
    directives: [ROUTER_DIRECTIVES],
})

export class HeaderComponent {
    public searchPhrase:string = '';

    constructor(private loginService:LoginService, private loginEvent:LoginEvent, private broadcaster:Broadcaster, private addArticleService:AddArticleService, private router: Router) {
        this.loginEvent.subscribe(value => this.showLogout = value);

    }

    public showLogout:boolean = false;
    showLogout = this.loginService.isLogged();

    public search() {
        console.log('Searching ' + this.searchPhrase);
        this.broadcaster.emit(this.searchPhrase);
    }

    public logout() {
        this.loginService.logout().subscribe(
            resp => this.showLogout = false,
            error => console.log('Logout problem')
        );
    }

    public addEmptyArticle() {
        // new, empty article
        var article:Article = <Article>{};
        this.addArticleService.addArticle(article).subscribe(
            res => this.router.navigate(['ArticleDetail', {url: res}]),
            error => alert(JSON.parse(error._body).error.message));
    }
}