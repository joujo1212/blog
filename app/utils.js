System.register([], function(exports_1) {
    "use strict";
    /**
     * Function return Json RPC 2.0 styled request in String
     * @param method method name, e.g. 'getAllArticles' (see server docs)
     * @param {Object} params in JSON object
     * @returns {string} Returns string prepared for send to server
     */
    function wrapJsonRPC(method, params) {
        return JSON.stringify({
            "method": method,
            "id": new Date().getTime(),
            "params": params
        });
    }
    exports_1("wrapJsonRPC", wrapJsonRPC);
    return {
        setters:[],
        execute: function() {
        }
    }
});
//# sourceMappingURL=utils.js.map