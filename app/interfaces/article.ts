export interface Article {
    id: number;
    title: string;
    perex: string;
    text: string;
    url: string;
    rating: number;
    img: string;
    tags: any;
    enabled: boolean;
}