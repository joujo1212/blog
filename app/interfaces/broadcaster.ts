// If in one file are more than one class, Injections in connstructors have to be in this form:
// @Inject(forwardRef(() => Broadcaster)) private broadcaster
import {EventEmitter} from 'angular2/core';
import {Injectable} from "angular2/core";

@Injectable()
export class Broadcaster extends EventEmitter {
    constructor() {
        super(true);
    }
}