System.register([], function(exports_1) {
    "use strict";
    var TITLE_POSTFIX, MAIN_PAGE_TITLE, SERVER_URL, ENDPOINT_UPLOAD_IMAGE;
    return {
        setters:[],
        execute: function() {
            exports_1("TITLE_POSTFIX", TITLE_POSTFIX = ' | Blog orenic.me');
            exports_1("MAIN_PAGE_TITLE", MAIN_PAGE_TITLE = 'Blog orenic.me');
            exports_1("SERVER_URL", SERVER_URL = 'http://localhost/blog-be/www/rest/request');
            exports_1("ENDPOINT_UPLOAD_IMAGE", ENDPOINT_UPLOAD_IMAGE = 'http://localhost/blog-be/www/image/upload');
        }
    }
});
//# sourceMappingURL=constants.service.js.map