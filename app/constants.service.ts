export var TITLE_POSTFIX: string = ' | Blog orenic.me';
export var MAIN_PAGE_TITLE: string = 'Blog orenic.me';
export var SERVER_URL = 'http://localhost/blog-be/www/rest/request';
export var ENDPOINT_UPLOAD_IMAGE = 'http://localhost/blog-be/www/image/upload';