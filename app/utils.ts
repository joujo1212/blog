/**
 * Function return Json RPC 2.0 styled request in String
 * @param method method name, e.g. 'getAllArticles' (see server docs)
 * @param {Object} params in JSON object
 * @returns {string} Returns string prepared for send to server
 */
export function wrapJsonRPC(method:string, params:Object) {
    return JSON.stringify(
        {
            "method": method,
            "id": new Date().getTime(),
            "params": params
        });
}